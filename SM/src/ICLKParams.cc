#include "mtf/SM/ICLKParams.h"

#define ICLK_MAX_ITERS 10
#define ICLK_EPSILON 0.01
#define ICLK_HESS_TYPE 0
#define ICLK_SEC_ORD_HESS false
#define ICLK_UPDATE_SSM false
#define ICLK_CHAINED_WARP false
#define ICLK_LEVEN_MARQ false
#define ICLK_LM_DELTA_INIT 0.01
#define ICLK_LM_DELTA_UPDATE 10
#define ICLK_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

ICLKParams::ICLKParams(int _max_iters, double _epsilon,
HessType _hess_type, bool _sec_ord_hess,
bool _update_ssm, bool _chained_warp,
bool _leven_marq, double _lm_delta_init,
double _lm_delta_update, bool _debug_mode):
max_iters(_max_iters),
epsilon(_epsilon),
hess_type(_hess_type),
sec_ord_hess(_sec_ord_hess),
update_ssm(_update_ssm),
chained_warp(_chained_warp),
leven_marq(_leven_marq),
lm_delta_init(_lm_delta_init),
lm_delta_update(_lm_delta_update),
debug_mode(_debug_mode){}

ICLKParams::ICLKParams(const ICLKParams *params) :
max_iters(ICLK_MAX_ITERS),
epsilon(ICLK_EPSILON),
hess_type(static_cast<HessType>(ICLK_HESS_TYPE)),
sec_ord_hess(ICLK_SEC_ORD_HESS),
update_ssm(ICLK_UPDATE_SSM),
chained_warp(ICLK_CHAINED_WARP),
leven_marq(ICLK_LEVEN_MARQ),
lm_delta_init(ICLK_LM_DELTA_INIT),
lm_delta_update(ICLK_LM_DELTA_UPDATE),
debug_mode(ICLK_DEBUG_MODE){
	if(params){
		max_iters = params->max_iters;
		epsilon = params->epsilon;
		hess_type = params->hess_type;
		sec_ord_hess = params->sec_ord_hess;
		update_ssm = params->update_ssm;
		chained_warp = params->chained_warp;
		leven_marq = params->leven_marq;
		lm_delta_init = params->lm_delta_init;
		lm_delta_update = params->lm_delta_update;
		debug_mode = params->debug_mode;
	}
}
const char*  ICLKParams::toString(HessType hess_type){
	switch(hess_type){
	case HessType::InitialSelf:
		return "Initial Self";
	case HessType::CurrentSelf:
		return "Current Self";
	case HessType::Std:
		return "Standard";
	default:
		throw std::invalid_argument(
			cv::format("ICLKParams :: Invalid hessian type provided: %d", hess_type));
	}
}

_MTF_END_NAMESPACE


